from setuptools import setup, find_packages

setup(
    name='uncle_ben',
    scripts=["uncle-ben.py"],
    packages=find_packages(),
    description='Uncle Ben',
    use_scm_version=True,
    author='Cristiano Werner Araujo',
    setup_requires=["setuptools_scm"],
    author_email="cristianowerneraraujo@gmail.com",
    url='',
    classifiers=[],
    install_requires=["pydantic", "python-dateutil", "radon", "pylama",
                      "pip-tools", "pip-package-list"],
    include_package_data=True
)
