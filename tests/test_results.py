import pytest

from uncle_ben.results import Results


@pytest.fixture
def result_sample():
    return [
        ("repo1", {
            "metric1": 1,
            "metric2": 1,
            "metric3": 1,
            "metric4": 1,
            "long metric4": 1,
            "long value metric": 1.55555555555555555555555555555555,
        }),
        ("long repo name", {
            "metric1": 1,
            "metric2": 1,
            "metric3": 1,
            "metric4": 1,
            "long metric4": 1,
            "long value metric": 1.131313131313131313131313131313131313,
        })
    ]


def test_result_string(result_sample):
    results = Results()
    for repo, result in result_sample:
        results.add(repo, result)
    expected = \
        """+-------------------+--------------------+--------------------+
|            metric |              repo1 |     long repo name |
+-------------------+--------------------+--------------------+
|           metric1 |                  1 |                  1 |
|           metric2 |                  1 |                  1 |
|           metric3 |                  1 |                  1 |
|           metric4 |                  1 |                  1 |
|      long metric4 |                  1 |                  1 |
| long value metric | 1.5555555555555556 | 1.1313131313131313 |
+-------------------+--------------------+--------------------+
"""
    assert expected == results.string()


def test_csv(result_sample):
    results = Results()
    for repo, result in result_sample:
        results.add(repo, result)
    expected =\
        """metric,repo1,long repo name
metric1,1,1
metric2,1,1
metric3,1,1
metric4,1,1
long metric4,1,1
long value metric,1.5555555555555556,1.1313131313131313
"""
    assert expected == results.csv()


def test_json(result_sample):
    results = Results()
    for repo, result in result_sample:
        results.add(repo, result)
    expected = """{
    "long repo name": {
        "long metric4": 1,
        "long value metric": 1.1313131313131313,
        "metric1": 1,
        "metric2": 1,
        "metric3": 1,
        "metric4": 1
    },
    "repo1": {
        "long metric4": 1,
        "long value metric": 1.5555555555555556,
        "metric1": 1,
        "metric2": 1,
        "metric3": 1,
        "metric4": 1
    }
}"""
    assert expected == results.json()
