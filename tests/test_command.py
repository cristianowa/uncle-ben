import pytest

import uncle_ben.command


def test_cmd(mocker):
    getstatusoutput = mocker.patch('uncle_ben.command.getstatusoutput')
    getstatusoutput.return_value = (0, "Ok!")
    response = uncle_ben.command.cmd("ls")
    assert response == "Ok!"


def test_cmd_fail(mocker):
    getstatusoutput = mocker.patch('uncle_ben.command.getstatusoutput')
    getstatusoutput.return_value = (1, "Ups!")
    with pytest.raises(Exception):
        uncle_ben.command.cmd("ls")
