import logging

from uncle_ben.logging import enable_debug, logger


def test_enable_debug():
    assert logger.level == logging.INFO
    enable_debug()
    assert logger.level == logging.DEBUG
