from uncle_ben.repository import Repository
from uncle_ben.results import Results


def test_git_target():
    results = Results()
    target = "https://gitlab.com/cristianowa/uncle-ben.git"
    repository = Repository(target)
    results.add(target, repository.complete_analysis())
    result = results.string()
    assert target in result
    assert "mccabe" in result
