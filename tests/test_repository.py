import pytest

from uncle_ben.repository import is_git_url


@pytest.mark.parametrize(
    "url, is_git",
    [
     ("https://example.com/gitproject.git", True),
     ("https://user@example.com/gitproject.git", True),
     ("ssh://user@server/project.git", True),
     ("ssh://server/project.git", True),
     ("git@github.com:user/project.git", True),
     ("github.com:user/project.git", True),
     ("https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols", False),
     ("https://github.com/project/project", False)
     ]
)
def test_is_git_url(url, is_git):
    assert is_git_url(url) == is_git
