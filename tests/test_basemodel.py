from enum import Enum

from uncle_ben.basemodel import BaseModel


def test_base_model_dict():
    class TestEnum(Enum):
        A = 'a'
        B = 'b'

    class TestModel(BaseModel):
        integer: int
        enumeration: TestEnum

    tm = TestModel(**{"integer": 1,
                      "enumeration": 'a'})
    result_dict = tm.dict()
    assert result_dict["test_model_integer"] == 1
    assert result_dict["test_model_enumeration"] == "a"
