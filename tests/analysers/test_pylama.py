import pytest

import uncle_ben.command
import uncle_ben.analysers.pylama


@pytest.fixture
def pylama_output(mocker):
    cmd = mocker.patch('uncle_ben.analysers.pylama.cmd')
    cmd.side_effect = ["""sample.py:1:1: D100 Missing docstring in public module [pep257]
sample.py:1:1: D100 Missing docstring in public module [pydocstyle]
sample.py:1:1: E265 block comment should start with '# ' [pep8]
sample.py:1:1: E265 block comment should start with '# ' [pycodestyle]
sample.py:1:0: C0114 Missing module docstring [pylint]
sample.py:3:1: E265 block comment should start with '# ' [pep8]
sample.py:3:1: E265 block comment should start with '# ' [pycodestyle]
sample.py:4:0: C0103 Constant name "a" doesn't conform to UPPER_CASE naming style [pylint]
sample.py:5:1: E265 block comment should start with '# ' [pep8]
sample.py:5:1: E265 block comment should start with '# ' [pycodestyle]
sample.py:7:1: D103 Missing docstring in public function [pep257]
sample.py:7:1: D103 Missing docstring in public function [pydocstyle]
sample.py:7:1: E302 expected 2 blank lines, found 1 [pep8]
sample.py:7:1: E302 expected 2 blank lines, found 1 [pycodestyle]
sample.py:7:0: C0104 Disallowed name "foo" [pylint]
sample.py:7:0: C0103 Argument name "x" doesn't conform to snake_case naming style [pylint]
sample.py:7:0: C0103 Argument name "y" doesn't conform to snake_case naming style [pylint]
sample.py:7:0: C0103 Argument name "z" doesn't conform to snake_case naming style [pylint]
sample.py:7:0: C0116 Missing function or method docstring [pylint]
sample.py:11:5: E265 block comment should start with '# ' [pep8]
sample.py:11:5: E265 block comment should start with '# ' [pycodestyle]
sample.py:14:1: D103 Missing docstring in public function [pep257]
sample.py:14:1: D103 Missing docstring in public function [pydocstyle]
sample.py:14:1: E302 expected 2 blank lines, found 1 [pep8]
sample.py:14:1: E302 expected 2 blank lines, found 1 [pycodestyle]
sample.py:14:1: W0404 redefinition of unused 'foo' from line 7 [pyflakes]
sample.py:14:0: E0102 function already defined line 7 [pylint]
sample.py:14:0: C0104 Disallowed name "foo" [pylint]
sample.py:14:0: C0116 Missing function or method docstring [pylint]
sample.py:15:9: W292 no newline at end of file [pep8]
sample.py:15:9: W292 no newline at end of file [pycodestyle]
sample.py:15:0: C0304 Final newline missing [pylint]""" # noqa
        , "", "", "", "", "", ""]


@pytest.mark.parametrize(
    'entry', [
        "sample.py:14:1: W0404 redefinition of unused 'foo' from line 7 [pyflakes]", # noqa
        "sample.py:14:0: E0102 function already defined line 7 [pylint]",
        "sample.py:14:1: E302 expected 2 blank lines, found 1 [pep8]",
        "sample.py:11:5: E265 block comment should start with '# ' [pycodestyle]", # noqa
    ]
)
def test_find_entries(entry):
    PyLamaAnalyser = uncle_ben.analysers.pylama.PyLamaAnalyser # noqa
    assert len(PyLamaAnalyser.find_outputs(entry)) == 1


def test_pylama(pylama_output):
    pylama = uncle_ben.analysers.pylama.PyLamaAnalyser()
    pylama_result = pylama.consolidate()
    assert pylama_result.pyflakes == 1
    assert pylama_result.mccabe == 0
    assert pylama_result.pycodestyle == 7
    assert pylama_result.pep257 == 3
    assert pylama_result.pydocstyle == 3
    assert pylama_result.pep8 == 7
    assert pylama_result.pylint == 11
