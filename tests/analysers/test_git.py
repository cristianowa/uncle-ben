import uncle_ben.command
import uncle_ben.analysers.git


COMMITS = """cristianowerneraraujo@gmail.com|Sun Apr 11 09:06:41 2021 -0300
cristianowerneraraujo@gmail.com|Tue Nov 3 21:39:21 2020 -0300
cristianowerneraraujo@gmail.com|Tue Nov 3 21:38:56 2020 -0300
cristianowerneraraujo@gmail.com|Tue Nov 3 21:38:46 2020 -0300"""

MERGES = """cristianowerneraraujo@gmail.com|Sun Apr 11 09:06:41 2021 -0300
cristianowerneraraujo@gmail.com|Tue Nov 3 21:39:21 2020 -0300"""


def test_git_commits(mocker):
    cmd = mocker.patch('uncle_ben.analysers.git.cmd')
    cmd.return_value = COMMITS
    git = uncle_ben.analysers.git.GitAnalyser()
    assert len(git.commits) == 4
    assert git.number_of_commits() == 4
    assert git.last_commit().day == 11
    assert git.first_commit().day == 3
    assert git.number_of_committers() == 1


def test_git_merges(mocker):
    cmd = mocker.patch('uncle_ben.analysers.git.cmd')
    cmd.return_value = MERGES
    git = uncle_ben.analysers.git.GitAnalyser()
    assert len(git.merges) == 2
    assert git.number_of_merges() == 2


def test_consolidate(mocker):
    cmd = mocker.patch('uncle_ben.analysers.git.cmd')
    cmd.side_effect = [
        COMMITS, MERGES
    ]
    git = uncle_ben.analysers.git.GitAnalyser()
    consolidated = git.consolidate()
    assert consolidated.number_of_commits == 4
    assert consolidated.number_merges == 2
