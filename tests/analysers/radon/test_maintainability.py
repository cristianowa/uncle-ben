"""{"setup.py": , "tests/test_postman_entry.py": """

import pytest

import uncle_ben.command
from uncle_ben.analysers.radon.maintainability import MaintainabilityAnalyser


@pytest.fixture
def radon_maintainability_output(monkeypatch):
    def _getstatusoutput(*args, **kwargs):
        return 0, """{
        "file1.py": {"mi": 100.0, "rank": "A"},
        "file2.py": {"mi": 47.3, "rank": "B"},
        "file3.py": {"mi": 23.2, "rank": "C"}
          }"""

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusoutput)
    return _getstatusoutput()[1]


@pytest.fixture
def radon_maintainability_output_empty(monkeypatch):
    def _getstatusoutput(*args, **kwargs):
        return 0, "{}"

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusoutput)
    return _getstatusoutput()[1]


def test_maintainability_decoding(radon_maintainability_output):
    maintainability = MaintainabilityAnalyser()
    output = maintainability.get_output()
    assert "file1.py" in output
    assert output["file1.py"]["mi"] == 100.0
    assert "file3.py" in output
    assert output["file3.py"]["rank"] == "C"


def test_maintainability_consolidation(radon_maintainability_output):
    maintainability = MaintainabilityAnalyser()
    consolidation = maintainability.consolidate()
    assert round(consolidation.mean_maintainability, 2) == 56.83
    assert round(consolidation.stddev_maintainability, 2) == 39.28
    assert consolidation.max_maintainability == 100.0
    assert consolidation.total_maintainability == 170.5
    assert consolidation.rank.value == "B"


def test_maintainability_consolidation_empty(
        radon_maintainability_output_empty):
    maintainability = MaintainabilityAnalyser()
    consolidation = maintainability.consolidate()
    assert consolidation.rank.value == "A"
    assert consolidation.mean_maintainability == 0
    assert consolidation.stddev_maintainability == 0
    assert consolidation.max_maintainability == 0
    assert consolidation.total_maintainability == 0
