import pytest

import uncle_ben.command
from uncle_ben.analysers.radon.raw import RawAnalyser, RawMetrics


@pytest.fixture
def radon_raw_output(monkeypatch):
    def _getstatusout(*args, **kwargs):
        return 0, """{
        "file1.py": {"loc": 31, "lloc": 1, "sloc": 21, "comments": 1, "multi": 0, "blank": 2, "single_comments": 1},
        "file2.py": {"loc": 32, "lloc": 2, "sloc": 22, "comments": 1, "multi": 0, "blank": 2, "single_comments": 1},
        "file3.py": {"loc": 33, "lloc": 3, "sloc": 23, "comments": 1, "multi": 0, "blank": 2, "single_comments": 1},
        "file4.py": {"loc": 34, "lloc": 4, "sloc": 24, "comments": 1, "multi": 0, "blank": 2, "single_comments": 1}
        }
        """  # noqa

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusout)
    return _getstatusout()[1]


@pytest.fixture
def radon_raw_output_empty(monkeypatch):
    def _getstatusout(*args, **kwargs):
        return 0, "{}"

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusout)
    return _getstatusout()[1]


def test_raw_decoding(radon_raw_output):
    raw = RawAnalyser()
    output = raw.get_output()
    print(output)
    assert "file1.py" in output
    assert output["file1.py"]["loc"] == 31
    assert output["file2.py"]["loc"] == 32
    assert output["file3.py"]["loc"] == 33


def test_raw_consolidate(radon_raw_output):
    raw = RawAnalyser()
    consolidated = raw.consolidate()
    assert consolidated.lloc == 10


def test_raw_consolidate_empty(radon_raw_output_empty):
    raw = RawAnalyser()
    consolidated = raw.consolidate()
    assert consolidated.loc == 0


def test_raw_metrics_sum():
    metric1 = RawMetrics(loc=1,
                         lloc=1,
                         sloc=1,
                         comments=1,
                         multi=1,
                         blank=1,
                         single_comments=1)
    metric2 = RawMetrics(loc=1,
                         lloc=1,
                         sloc=1,
                         comments=1,
                         multi=2,
                         blank=1,
                         single_comments=1)
    summed_metric = metric1 + metric2
    assert summed_metric.loc == 2
    assert summed_metric.multi == 3
