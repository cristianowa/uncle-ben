import pytest

import uncle_ben.command
from uncle_ben.analysers.radon.mccabe import McCabeAnalyser


@pytest.fixture
def radon_mccabe_output(monkeypatch):
    def _getstatusout(*args, **kwargs):
        return 0, """{
        "file1.py": [{"type": "function", "rank": "A", "col_offset": 0,
             "lineno": 10, "complexity": 2, "name": 
             "file1", "endline": 10, "closures": []},
              {"type": "function", "rank": "A", "col_offset": 0, "lineno": 103, "complexity": 2, "name": 
              "test_postman_entry_create", "endline": 103, "closures": []}, 
              {"type": "function", "rank": "A", "col_offset": 0, "lineno": 108, 
              "complexity": 2, "name": "test_entry_repr", "endline": 108, "closures": []}],
          "file2.py": [{"type": "function", "rank": "C", "col_offset": 0,
                        "lineno": 4, "complexity": 3, "name": "test_folder",
                         "endline": 4, "closures": []}] 
            }""" # noqa

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusout)
    return _getstatusout()[1]


def test_mccabe_decoding(radon_mccabe_output):
    raw = McCabeAnalyser()
    output = raw.get_output()
    print(output)
    assert "file1.py" in output
    assert output["file1.py"][0]["rank"] == "A"


def test_mccabe_consolidation(radon_mccabe_output):
    mccabe = McCabeAnalyser()
    consolidation = mccabe.consolidate()
    assert consolidation.total_complexity == 9
    assert consolidation.max_complexity == 3
    assert consolidation.mean_complexity == 2.25
    assert consolidation.stddev_complexity == 0.5
    assert consolidation.rank.value == "B"
