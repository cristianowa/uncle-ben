"""{"setup.py": , "tests/test_postman_entry.py": """

import pytest

import uncle_ben.command
from uncle_ben.analysers.radon.halstead import HalsteadAnalyser


@pytest.fixture
def radon_halstead_output(monkeypatch): # noqa
    def _getstatusout(*args, **kwargs):
        return 0, """{ 
        "file1.py": {"total": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0, 0.0], "functions": []},
        "file2.py": {"total": [2, 6, 3, 6, 8, 9, 17.509775004326936, 27.0, 1.0, 27.0, 1.5, 0.009], 
                     "functions": [
                          ["function1", [1, 2, 1, 2, 3, 3, 2.0, 4.754887502163469, 0.5, 2.37, 0.13, 0.0015]], 
                          ["function2", [1, 2, 1, 2, 3, 3, 2.0, 4.75, 0.5, 2.37, 0.132, 0.001]], 
                          ["function3", [1, 2, 1, 2, 3, 3, 2.0, 4.759, 0.5, 2.3774, 0.132080, 0.0015]]
                          ]
                    },
         "file3.py": {"total": [2, 3, 2, 4, 5, 6, 6.75, 13.9315, 1.33, 18.57, 1.03, 0.0046],
                      "functions": [["__init__", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0, 0.0]],
                                    ["name", [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0, 0.0]],
                                    ["requests", [2, 3, 2, 4, 5, 6, 6.75, 13.9315, 1.333, 18.575, 1.03, 0.0046]]
                                    ]
                      }
          }""" # noqa

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusout)
    return _getstatusout()[1]


@pytest.fixture
def radon_halstead_output_empty(monkeypatch):
    def _getstatusout(*args, **kwargs):
        return 0, "{}"

    monkeypatch.setattr(uncle_ben.command, "getstatusoutput", _getstatusout)
    return _getstatusout()[1]


def test_halstead_decoding(radon_halstead_output):
    halstead = HalsteadAnalyser()
    output = halstead.get_output()
    print(output)
    assert "file1.py" in output
    assert len(output["file1.py"]["total"]) == 12
    assert "file3.py" in output
    assert output["file3.py"]["total"][0] == 2


def test_halstead_consolidation(radon_halstead_output):
    halstead = HalsteadAnalyser()
    consolidation = halstead.consolidate()
    assert consolidation.n1 == 4
    assert consolidation.n2 == 9
    assert round(consolidation.volume) == 56


def test_mccabe_consolidation_empty(radon_halstead_output_empty):
    halstead = HalsteadAnalyser()
    consolidation = halstead.consolidate()
    assert consolidation.n1 == 0
    assert consolidation.n2 == 0
    assert round(consolidation.volume) == 0
